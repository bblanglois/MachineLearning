%matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

# Load training and testing data
X_train = np.loadtxt('X_train.csv', delimiter=',', skiprows=1)
X_test = np.loadtxt('X_test.csv', delimiter=',', skiprows=1)
y_train = np.loadtxt('y_train.csv', delimiter=',', skiprows=1)[:,1]

X = np.hstack((np.ones((168,1)),X_train,X_train**2))
newXTest = np.hstack((np.ones((41,1)),X_test,X_test**2))

XX = np.dot(X.T,X)
invXX = np.linalg.pinv(XX)
Xt = np.dot(X.T,y_train)
wVect = np.dot(invXX,Xt)

testt = np.dot(newXTest,wVect)

plt.figure()
plt.scatter(np.arange(0, 41), testt)

tXw = y_train - np.dot(X, wVect)
sigma2 = np.dot(tXw.T, tXw) / len(y_train)

print(sigma2)

test_header = "Id,PRP"
n_points = testt.shape[0]
y_pred_pp = np.ones((n_points, 2))
y_pred_pp[:, 0] = range(n_points)
y_pred_pp[:, 1] = testt
print(y_pred_pp)
np.savetxt('my_submission.csv', y_pred_pp, fmt='%d,%f', delimiter=",",
           header=test_header, comments="")