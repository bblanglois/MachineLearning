import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv), data manipulation as in SQL
import matplotlib.pyplot as plt # this is used for the plot the graph 
%matplotlib inline
from sklearn.model_selection import GridSearchCV# for tuning parameter
from sklearn.naive_bayes import GaussianNB
import numpy as np
import pylab as plt
%matplotlib inline
from sklearn.model_selection import train_test_split

X_train = np.loadtxt('train7best.csv',delimiter=";",skiprows=1)
Y_train = np.loadtxt('y_train.csv',delimiter=",",skiprows=1)
X_test = np.loadtxt('test7best.csv',delimiter=";",skiprows=1)


data = pd.read_csv('test7best.csv',header=0,delimiter=";")
datatest= pd.read_csv('train7best.csv', header=0,delimiter=";")

        

train_X_final, testTR = train_test_split(data, test_size = 0.0)
test_X_final, testTST = train_test_split(datatest, test_size = 0.0)


y_Test = np.zeros(shape=(1596,2))

for i in range(1596):
    y_Test[i] = np.array([i,0])
    
params = {}
for cl in range(1,3):
    data_pos = np.where(Y_train==cl)[0]
    class_pars = {}
    class_pars['mean'] = X_train[data_pos,:].mean(axis=0)
    class_pars['vars'] = X_train[data_pos,:].var(axis=0)
    class_pars['prior'] = 1.0*len(data_pos)/len(X_train)
    params[cl] = class_pars
    
print (X_test.shape)
print (X_train.shape)

predictions = np.zeros((1596,))


for j,tx in enumerate(X_test):
    un_norm_prob = np.zeros((21,))
    for cl in params:
        un_norm_prob[cl] = params[cl]['prior']
        for i,m in enumerate(params[cl]['mean']):
            vari = params[cl]['vars'][i]
            un_norm_prob[cl] *= 1.0/(np.sqrt(2.0*np.pi*vari))
            un_norm_prob[cl] *= np.exp((-0.5/vari)*(tx[i]-m)**2)
            
    norm_prob = un_norm_prob/(un_norm_prob.sum())
    predictions[j] = norm_prob.argmax()

print (predictions)

test_header = "Id,EpiOrStroma"
print (predictions.shape)
n_points = X_test.shape[0]
y_pred_pp = np.ones((n_points, 2))
y_pred_pp[:, 0] = range(n_points)
y_pred_pp[:, 1] = predictions[:]

print (y_pred_pp)
np.savetxt('classificationSub.csv', y_pred_pp, fmt='%d', delimiter=",",
           header=test_header, comments="")