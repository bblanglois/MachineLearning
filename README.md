# Introduction

This project was done as part of a Machine Learning Class (COMPSCI4061) in 2018 at the University of Glasgow. The PDF document provided by the course's teachers describing the coursework has been included in the repository.  It would be useful to read it to understand the purpose and the nature of the tasks required. The project initially had to be runnable on the Jupyter Notebook framework in Python (.ipynb format), but for both classification and regression tasks, Python source code files (.py) and Python Notebook files have been provided.  You can learn how to easily run the Jupyter Notebook framework [here](https://ipython.org/notebook.html).
This coursework came in the form of two distinct Kaggle competitions like mentionned in the ml_cousrework_2018b.pdf file : one for the [regression task](https://www.kaggle.com/c/uog-ml-2018-r) and one for the [classification task](https://www.kaggle.com/c/uog-ml-2018-c).


# Regression 


### 1. Polynomial Regression



##### Introduction :
Polynomial regression is an extension of linear regression which uses higher order polynomials in contrast to simple linear polynomials. Through plenty of research it seems that for datasets involving more than 1 or 2 features, polynomial regression seems to be more effective than linear regression as in a lot of cases, there is not a simple linear relationship between the test data and the training data, which seemed to occur in this scenario. Given the training data, consisting of 168 CPU’s (having 6 features) and a corresponding CPU performance in orders of magnitude 10. The linear regression method assumes a linear relationship between the 6 different features and the CPU performance, which was problematic. There seemed to be a more complex relationship than linear between the training data and test data for the training data, thus we considered polynomial regression. Initially the coefficients to the polynomial will be unknown, and the main purpose of polynomial regression is to determine those coefficients. By adopting a ‘minimum loss’ process, the coefficients are determined well enough to obtain a unique polynomial, and then this polynomial can be used on the test data to obtain measures for the CPU performance.

##### Data features and initial thoughts:

Given 168 rows of training data with each row having stats of 6 features and a CPU performance measure for each row, it made sense to analyse the features first of all. We started with the machine cycle time feature and obtained the following ‘relationship’ between the machine cycle time and CPU performance:

![](Images/Capture d’écran 2018-06-25 à 22.44.21.png)

Where the x-axis represents the machine cycle time and the y-axis represents the CPU performance. Having a fairly good knowledge of mathematics, we noticed right away this resembled the function **f(X)=1/X**.

We kept this in mind as we analysed the rest of the features. Unfortunately with the rest of the features there was no strong correlation so we went ahead with implementing the algorithm.

##### Experimental Setup and Procedure:


We started off by loading the training data and test file from the given csv file as follows:
```python
X_train = np.loadtxt('X_train.csv', delimiter=',', skiprows=1)
X_test = np.loadtxt('X_test.csv', delimiter=',', skiprows=1)
y_train = np.loadtxt('y_train.csv', delimiter=',', skiprows=1)[:,1]
```
With the skiprows used to skip the headings. After this we proceeded to construct a matrixconsisting of a polynomial or order 2. We also attempted to add some matrix terms to the power of -1 to keep consistent with the resemblance **f(X) = 1/X** , however the lowest we could get the score was ~42.5 which wasn’t quite enough. We also tried using a matrix consisting of the training data in each column boosted to a different power of  **i/2** with i ranging from 0 to n. Unfortunately this didn’t improve the score majorly, so we attempted to construct a matrix consisting of powers from i=1 to 2:
```python
X = np.hstack((np.ones((168,1)),X_train,X_train**2))
```
After this, we continued to use the method from lectures to calculate the vector for the coefficients, which involved the following equation :

![](Images/Capture d’écran 2018-06-25 à 22.44.44.png)


This equation is used to calculate the best coefficient matrix such that the loss for our model is
minimised.
We calculated this as follows:
```python
XX = np.dot(X.T,X)
invXX = np.linalg.pinv(XX)
Xt = np.dot(X.T,y_train)
wVect = np.dot(invXX,Xt)
```

After this we were able to use this coefficient vector to calculate an approximate CPU
performance for all the test values:

```python
newXTest = np.hstack((np.ones((41,1)),X_test,X_test**2))
testt = np.dot(newXTest,wVect)
```

Where the top line is just constructing a matrix similar to the previous matrix.From this, we tested our calculated values of the CPU performance and managed to obtain a score of ~36.

##### Extra experimentation: 
Since we had reached a score of **\< 42**, we decided to experiment with coupling some features. We decided to couple the max and min memory features be taking the average of them andre-calculated the X vector. Although when we attempted the whole process again, we didn’t manage to lower our score any more. From this we decided to move onto our different model since we have reached the best score threshold anyway.

### 2. Bayesian Regression

##### Introduction: 
Bayesian Regression is becoming extremely important within machine learning and we have considered to use this model for our second implementation. Given the training data, we were able to compute the prior distribution and likelihood over these values and then use Bayes’ rule to see how the density changes as we add evidence from the observed data. The resulting posterior density can then be examined and used to compute predictions. Within this section I will go through the steps of using Bayes rule to compute predictions of the 168 CPU training data values we were given.

##### Data features and initial thoughts: 

As mentioned above we were given 168 rows of training data with each row having stats of 6 features and a CPU performance measure for each row. We then tried to combine some of the features such as the min and max memory by taking the average/sum/difference between these two columns and constructing a new training data matrix. However we realised that this didn’t help to improve our model so we stuck to using the original training matrix given.

##### Experimental Setup and Procedure:

We started off by loading the three csv files as above to be used in our implementation and then plotting the x train value against the y train values as follows:

![](Images/Capture d’écran 2018-06-25 à 23.42.00.png)

From our previous model we were able to determine the sigma squared value of our model as follows:
```python
tXw = y_train - np.dot(X, wVect)
sigma2 = np.dot(tXw.T, tXw) / len(y_train)
```

This give back a result of **sigma2** being **2399.07091186** which we were then able to use to compute the prior distribution.

After this we constructed our X and Xtest matrices by:
```python
X = np.hstack((np.ones((168,1)),X_train,X_train**2,X_train**3))
Xtest = np.hstack((np.ones((41,1)),X_test,X_test**2,X_test**3))
```

We found that using a 3rd order polynomial for our training and test data worked well with our model by giving us a lower loss value and decided to use this to compute our prior distribution. Having constructed our X matrix we were then able to compute the prior distribution as follows. We have based this from the lab on the olympic bayesian model and incorporated it into our model.
```python
sig_w = np.linalg.inv((1.0/sig_sq)*np.dot(X.T,X) + np.linalg.inv(prior_cov))
mu_w = (1.0/sig_sq)*np.dot(sig_w,np.dot(X.T,y_train))
```

Where the value of prior_cov is computed by:
```python
prior_cov = 100.0*np.eye(l)
```

We were then able to plot the prior distribution:

![](Images/Capture d’écran 2018-06-25 à 23.06.09.png)

After being able to compute the prior distribution we were then able to fit this by computing the posterior density and then making predictions. We did this as follows:

```python
# posterior and compute predictions
predmu = np.dot(Xtest,mu_w) 
predvar = sig_sq + np.diag(np.dot(Xtest,np.dot(sig_w,Xtest.T)))
```

##### Extra experimentation:
After computing our predictions for this model we then decided to experiment other methods in order to improve our model. We had first tried to combine some of the features, as I mentioned above, however this did not give us a better score. We also tried to take some of the features out of the model, for example the machine cycle but this also didn’t give a better score in our predictions.

### Polynomial vs Bayesian regression
It is difficult to tell which regression performs better as both scores on kaggle are pretty similar. However we can see that the polynomial regression yields a lower score and only uses a second order polynomial meaning that it is a much less complex model. There is room for improvement in both models that could boost the performance greatly.

# Classification

### 1. Naive Bayes Classifier

##### Introduction: 
In Machine Learning, naive Bayes Classifiers forms a family of simple probabilistic classifier based on the Bayes’ theorem. These classifiers are used, assuming a strong independence between the different features that constitute the training and testing data, to predict a probability distribution over a set of these said features. A Gaussian distribution was used for the naive Bayes Classifier algorithm in this coursework. But before even calculating the mean values and the variances for every features, we needed to explore our data to make sure it was well suited for our Classifier.

##### Data Features: 
Naive Bayes Classifiers work by assuming a strong independence between the different data features as previously mentioned. We explored our data, with a colerration grid, to illustrate, as a matter of fact, if some of the 112 features had strong correlations with each other. The results showed us that a lot of our features were strongly correlated (see Figure 1). We got rid of a lot of these correlated features and trimmed them to only 38. Then, with the help of another figure representing the features importance in correlation with the “EpiOrStroma” results of the y_train.csv file, we were able to trim our data features to only 11, keeping the most deterministic and not correlated ones. We obtained what we consider as sufficiently good results, for this task, with this method. (This was done by testing many different attempts, of course. See more details in the Progression section for the score and choice evolution of the features).

![](Images/Capture d’écran 2018-06-25 à 23.44.50.png)

![](Images/Capture d’écran 2018-06-25 à 23.44.56.png)

##### Experimental Setup and Procedure:

**1.** Our classifier works by first calculating the mean value and the variance for each feature column in our dataset, for each particular class (in this case 1 and 2, or epithelial and stromal. The set of targets is located in the y_train.csv file, in the column “EpiOrStroma”). This allows us to fit a Gaussian distribution to each feature, for each class. We also compute the calculation of the prior probability distribution based on the set of targets. This value will be used later to calculate the probability of the test data sample predictions. These first steps are implemented in the code shown in the following code : 

```python
params = {}
for cl in range(1,3):
    data_pos = np.where(Y_train==cl)[0]
    class_pars = {}
    class_pars['mean'] = X_train[data_pos,:].mean(axis=0)
    class_pars['vars'] = X_train[data_pos,:].var(axis=0)
    class_pars['prior'] = 1.0*len(data_pos)/len(X_train)
    params[cl] = class_pars
```

**2.** With the mean values and the variances calculated for each features, for each class, we are now able to compute the values of the probability density of a Gaussian distribution for each class :

![](Images/Capture d’écran 2018-06-25 à 23.16.12.png)

By implementing this formula in a loop, we calculate the likelihood of every test points, and for each class. We have to note that we multiply the result of the Gaussian formula by the prior probability distribution on each iteration. See the following code :
```python
for j,tx in enumerate(X_test):
    un_norm_prob = np.zeros((21,))
    for cl in params:
        un_norm_prob[cl] = params[cl]['prior']
        for i,m in enumerate(params[cl]['mean']):
            vari = params[cl]['vars'][i]
            un_norm_prob[cl] *= 1.0/(np.sqrt(2.0*np.pi*vari))
            un_norm_prob[cl] *= np.exp((-0.5/vari)*(tx[i]-m)**2)
```

**3.** Finally, for each class, we normalize the likelihoods into probabilities for the two classes. The greatest class probability for every test point is our prediction for it. See the following code :
```python
norm_prob = un_norm_prob/(un_norm_prob.sum())
predictions[j] = norm_prob.argmax()
```

**4.** We then write our result in the format .csv file format for calculating our score on the Kaggle platform. See the following code :
```python
test_header = "Id,EpiOrStroma"
print (predictions.shape)
n_points = X_test.shape[0]
y_pred_pp = np.ones((n_points, 2))
y_pred_pp[:, 0] = range(n_points)
y_pred_pp[:, 1] = predictions[:]

print (y_pred_pp)
np.savetxt('classificationSub.csv', y_pred_pp, fmt='%d', delimiter=",",
           header=test_header, comments="")
```

##### Extra experimentation:

We first tested the classifier on the data provided without modifying it (with the 112 features). We scored a score of 0.82845 wich was decent. By getting rid of correlated data features with the correlation grid, we were able to score 0.84518 (38 features), then 0.84728 (36 features), and 0.84937(35 features). We then got rid of the less relevant data by using the features importance graph, and were able with three attempts to get up to 0.86820. By reducing every tries the number of features, cutting down the less important ones, we finished with only 11 (the test7best.csv and train7best.csv files represent the provided data cut down to only these 11 features).

### 2. Gradient Boosting Classifier

##### Introduction: 
Gradient boosting is a supervised machine learning technique which is based around the principle of ensemble. This process combines a set of weak learners which allows the classifier to improve the accuracy of its predictions. Boosting algorithms are gaining in popularity due to the effectiveness of the way they deal with the bias variance trade-off. The technique used for classification is similar to the technique used for regression.

##### Experimental Setup and Procedure:

To begin with, we imported the relevant files and imported the
GradientBoostingClassifier algorithm from the SKlearn package. The Gradient Boosting algorithm follows a structure as follows, starting with initializing the outcome. The next step is to iterate from 1 to the total number of trees. For each of these iterations, we update the weights of features based on the previous iterations. This is followed by fitting the model on a sample of the data, before making predictions on the full set of data, and updating the current results if the result achieved scores a better score than previous iterations. Unlike the method used in the naive Bayes Classifier, we selected all the features as input into our classifier to obtain the best possible score. After achieving a high score in with the naive Bayes Classifier, we only used all the features and still were able to obtain a high result. We would be able to lower this score further if we conducted further data analysis to determine which features deserve a higher weight, although this is already conducted in the package to a certain degree. By using the GridSearchCV method from the SKlearn package, we were able to determine the best parameters to run our Gradient Boosting classifier with. This led to us finding that the best parameter to run the classifier with was having our n_estimators to be 70. This increased our score to the final result of 0.78242, making this our second best method after naive Bayes.

### Naive Bayes Classifier vs Gradient Boosting Classifier:
Our naive Bayes Classifier achieved an impressive score of 0.86820, whereas our Gradient Boosting Classifier achieved a score of 0.78242. The naive Bayes classifier tackles the problem of classification in a simple probabilistic manner based upon Bayes Theorem. This is in contrast to the Gradient Boosting which is a more sequential model similar to regression in its operation. The main difference between our two implementations of these classifiers is that we cut down the number of features inputted into the naive Bayes classifier to 11 whereas we still inputted all the features into the Gradient Boosting classifier. We believe that it is in this difference in input features that lies the difference in the scores we obtained. In lowering the number of features in naive Bayes we tested and refined our model repeatedly, causing the gradual increase in our score to its final point. Once we had achieved this high score we felt it wasn’t necessary to cut down the inputs to the Gradient Boosting classifier.


By Benjamin Langlois 2018








   